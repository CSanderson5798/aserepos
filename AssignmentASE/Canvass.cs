﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssignmentASE
{
    /// <summary>
    /// The Canvas class contains information that the WindowsForm shows via the commands.
    /// </summary>
    class Canvass
    {
        Graphics g;     //'g' is the GDI surface.
        Pen p;          //'p' is the object that will be used to draw.
       
        int xPosition, yPosition;

        public Canvass(Graphics g)
        {
            this.g = g;                     //At execution, 'g' is the local variable.
            xPosition = yPosition = 0;      //Pen position.
            p = new Pen(Color.Black, 3);    //Standard pen.

        }

        /// <summary>
        /// Drawing a line from the pen position.
        /// </summary>
        /// <param name="toX"> x Position to draw to </param>
        /// <param name="toY"> y Position to draw to </param>
        public void DrawTo(int endX, int endY)
        {

            g.DrawLine(p, xPosition, yPosition, endX, endY);    //Draw Line.
            xPosition = endX;
            yPosition = endY;
            //Both x & y being updated as pen position has now moved from original position to end.
        }

        /// <summary>
        /// All pen colours were placed into seperate methods - the way to call upon them.
        /// </summary>
        public void PenColourRed()
        {
            p = new Pen(Color.Red, 3);        
        }
        public void PenColourBlack()
        {
            p = new Pen(Color.Black, 3);
        }
        public void PenColourGreen()
        {
            p = new Pen(Color.Green, 3);
        }
        public void PenColourBlue()
        {
            p = new Pen(Color.Blue, 3);
        }
        /// <summary>
        /// Fill the inside of the shape specified by a pair of coordinates, width and height.
        /// Variables given values where needed.
        /// </summary>
        public void FillRectangle(int width, int height)
        {
            SolidBrush recBrush = new SolidBrush(Color.Black);
            g.FillRectangle(recBrush, xPosition, yPosition, width, height);
        }

        /// <summary>
        /// SolidBrush from Drawing.Graphics.
        /// Fills the circle specified by a pair of coordinates and radius (calculation giving diameter).
        /// </summary>
        /// <param name="Radius"></param>
        public void FillEllipse(int Radius)
        {
            SolidBrush circBrush = new SolidBrush(Color.Black);
            g.FillEllipse(circBrush, xPosition, yPosition, xPosition + (Radius * 2), yPosition + (Radius * 2));
        }

        /// <summary>
        /// Clears the drawing surface that the user is placing shapes onto.
        /// </summary>
        public void clearCanvass()
        {
            xPosition = yPosition = 0;    //Reset pen position.
            p = new Pen(Color.Black, 3);  //Reset pen color to default.
            g.Clear(Color.Transparent);   //Clears drawing surface - giving it a transparent colour for visibility.

        }

        /// <summary>
        /// Reset the position on the Bitmap.
        /// </summary>
        public void positionReset()
        {
            xPosition = yPosition = 0;    //Reset pen position to Initial top left.
        }

        /// <summary>
        /// Defining an Array of points - storing points in which are x & y coordinates.
        /// </summary>
        public void FillTriangle()
        {
            SolidBrush triangleBrush = new SolidBrush(Color.Black);

            Point[] pnt = new Point[3];

            pnt[0].X = 150;         //Sets the x Cordinate of 1st Point.
            pnt[0].Y = 150;         //Sets the Y Cordinate of 1st Point.

            pnt[1].X = 100;         //Sets the X Cordinate of 2nd Point.
            pnt[1].Y = 200;         //Sets the Y Cordinate of 2nd Point.

            pnt[2].X = 45;          //Sets the X Cordinate of 3rd Point.
            pnt[2].Y = 150;         //Sets the Y Cordinate of 3rd Point.

            g.FillPolygon(triangleBrush, pnt);
        }

        /// <summary>
        /// Drawing a line in transparency for the user to move the current position.
        /// </summary>
        /// <param name="endX"></param>
        /// <param name="endY"></param>
        public void MoveTo(int endX, int endY)
        {
            p = new Pen(Color.Transparent, 3); //Pen is transparent to show movement without trailing line graphic.

            g.DrawLine(p, xPosition, yPosition, endX, endY);
            xPosition = endX;
            yPosition = endY;

          //  p.Dispose();                      //Disposing of the transparent pen.
          //  p = new Pen(Color.Black, 3);      //Set Pen back to default colour and width.

        }

        /// <summary>
        /// Draws a rectangle specified by coordinates, width and height.
        /// </summary>
        public void DrawRectangle(int width, int height)
        {
            g.DrawRectangle(p, xPosition, yPosition, width, height);    //Draw Rectangle.
        }

        /// <summary>
        /// Draws the shape specified by a pair of coordinates and parameter (calculation giving diameter).
        /// </summary>
        /// <param name="Radius"></param>
        public void DrawEllipse(int Radius)
        {
            g.DrawEllipse(p, xPosition, yPosition, xPosition + (Radius * 2), yPosition + (Radius * 2));
        }

        /// <summary>
        /// Defining an Array of points.
        /// </summary>
        public void DrawTriangle()
        {
            Point[] pnt = new Point[3];     

            pnt[0].X = 150;         //Sets the x Cordinate of 1st Point.
            pnt[0].Y = 150;         //Sets the Y Cordinate of 1st Point.

            pnt[1].X = 100;         //Sets the X Cordinate of 2nd Point.
            pnt[1].Y = 200;         //Sets the Y Cordinate of 2nd Point.

            pnt[2].X = 45;          //Sets the X Cordinate of 3rd Point.
            pnt[2].Y = 150;         //Sets the Y Cordinate of 3rd Point.

            g.DrawPolygon(p, pnt);

        }
    }
}
