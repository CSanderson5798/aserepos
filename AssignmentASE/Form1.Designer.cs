﻿
namespace AssignmentASE
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.exitButton = new System.Windows.Forms.Button();
            this.OutputPicturebox = new System.Windows.Forms.PictureBox();
            this.commandLine = new System.Windows.Forms.TextBox();
            this.ProgrammingTextbox = new System.Windows.Forms.RichTextBox();
            this.loadButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.OutputPicturebox)).BeginInit();
            this.SuspendLayout();
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(780, 662);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(129, 56);
            this.exitButton.TabIndex = 2;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // OutputPicturebox
            // 
            this.OutputPicturebox.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.OutputPicturebox.Location = new System.Drawing.Point(704, 12);
            this.OutputPicturebox.Name = "OutputPicturebox";
            this.OutputPicturebox.Size = new System.Drawing.Size(581, 508);
            this.OutputPicturebox.TabIndex = 3;
            this.OutputPicturebox.TabStop = false;
            this.OutputPicturebox.Paint += new System.Windows.Forms.PaintEventHandler(this.OutputPicturebox_Paint);
            // 
            // commandLine
            // 
            this.commandLine.Location = new System.Drawing.Point(409, 581);
            this.commandLine.Name = "commandLine";
            this.commandLine.Size = new System.Drawing.Size(581, 31);
            this.commandLine.TabIndex = 4;
            this.commandLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.commandLine_KeyDown);
            // 
            // ProgrammingTextbox
            // 
            this.ProgrammingTextbox.Location = new System.Drawing.Point(12, 12);
            this.ProgrammingTextbox.Name = "ProgrammingTextbox";
            this.ProgrammingTextbox.Size = new System.Drawing.Size(626, 508);
            this.ProgrammingTextbox.TabIndex = 5;
            this.ProgrammingTextbox.Text = "";
            this.ProgrammingTextbox.TextChanged += new System.EventHandler(this.ProgrammingTextbox_TextChanged);
            // 
            // loadButton
            // 
            this.loadButton.Location = new System.Drawing.Point(645, 663);
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(129, 56);
            this.loadButton.TabIndex = 8;
            this.loadButton.Text = "Load";
            this.loadButton.UseVisualStyleBackColor = true;
            this.loadButton.Click += new System.EventHandler(this.loadButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(509, 663);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(129, 56);
            this.saveButton.TabIndex = 9;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1323, 731);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.loadButton);
            this.Controls.Add(this.ProgrammingTextbox);
            this.Controls.Add(this.commandLine);
            this.Controls.Add(this.OutputPicturebox);
            this.Controls.Add(this.exitButton);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.OutputPicturebox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.PictureBox OutputPicturebox;
        private System.Windows.Forms.TextBox commandLine;
        private System.Windows.Forms.RichTextBox ProgrammingTextbox;
        private System.Windows.Forms.Button loadButton;
        private System.Windows.Forms.Button saveButton;
    }
}

