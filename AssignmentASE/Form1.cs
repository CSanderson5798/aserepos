﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssignmentASE
{
    public partial class Form1 : Form
    {
        Bitmap MyBitmap = new Bitmap(640, 480);  //New bitmap to draw what will appear - change this (numbers).
        Canvass MyCanvass;

        public Form1()
        {
            InitializeComponent();
            MyCanvass = new Canvass(Graphics.FromImage(MyBitmap));  //Passes the drawing surface to canvass object.

        }

        /// <summary>
        /// All commands that the user may use.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void commandLine_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string x, y;
                String Command = commandLine.Text.Trim().ToLower();     //Converts commandLine text to all lowercase and trims spaces.
                string[] Commands = Command.Split(',');

                string programBox = ProgrammingTextbox.Text.Trim().ToLower();
                string[] programLine = programBox.Split('\n');                  //Split the ProgramTextbox by new line.

                if (Commands[0] == ("run") == true)
                {

                   for (int i = 0; i < programLine.Length; i++)             //Incrementing by 1.
                    {
                       string[] pcommand = programLine[i].Split(',');       //Array for programline that will split for parameters.

                        if (pcommand[0] == ("drawto"))
                        {
                            try
                            {
                                x = pcommand[1];
                                y = pcommand[2];
                                int paramX = Convert.ToInt32(x);
                                int paramY = Convert.ToInt32(y);
                                MyCanvass.DrawTo(paramX, paramY);
                                Console.WriteLine("DRAWN");
                            }
                            catch
                            {
                                MessageBox.Show("INVALID DRAW PARAMETERS");
                            }
                        }
                        else if (pcommand[0] == ("moveto"))
                        {
                            try
                            {
                                x = pcommand[1];
                                y = pcommand[2];
                                int paramX = Convert.ToInt32(x);
                                int paramY = Convert.ToInt32(y);

                                MyCanvass.MoveTo(paramX, paramY);
                                MyCanvass.PenColourBlack();

                                Console.WriteLine("MOVED");
                            }
                            catch
                            {
                                MessageBox.Show("INVALID MOVE PARAMETERS");
                            }
                        }
                        else if (pcommand[0] == ("circle"))
                        {
                            try
                            {
                                x = pcommand[1];
                                int paramX = Convert.ToInt32(x);
                                MyCanvass.DrawEllipse(paramX);
                                Console.WriteLine("CIRCLE");
                            }
                            catch
                            {
                                MessageBox.Show("INVALID CIRCLE PARAMETERS");
                            }
                        }
                        else if (pcommand[0] == ("rectangle"))
                        {
                            try
                            {
                                x = pcommand[1];
                                y = pcommand[2];
                                int paramX = Convert.ToInt32(x);
                                int paramY = Convert.ToInt32(y);
                                MyCanvass.DrawRectangle(paramX, paramY);
                                Console.WriteLine("RECTANGLE");
                            }
                            catch
                            {
                                MessageBox.Show("INVALID RECTANGLE PARAMETERS");
                            }
                        }
                        else if (pcommand[0] == ("triangle"))
                        {

                            MyCanvass.DrawTriangle();
                            Console.WriteLine("TRIANGLE");
                        }
                        else if (pcommand[0] == ("reset"))
                        {
                            MyCanvass.positionReset();
                            Console.WriteLine("RESET PEN POSITION TO INITIAL POINT");
                        }
                        else if (pcommand[0] == ("penblue"))
                        {
                            MyCanvass.PenColourBlue();
                            Console.WriteLine("PEN BLUE");
                        }
                        else if (pcommand[0] == ("penred"))
                        {
                            MyCanvass.PenColourRed();
                            Console.WriteLine("PEN RED");
                        }
                        else if (pcommand[0] == ("penblack"))
                        {
                            MyCanvass.PenColourBlack();
                            Console.WriteLine("PEN BLACK");
                        }
                        else if (pcommand[0] == ("pengreen"))
                        {
                            MyCanvass.PenColourGreen();
                            Console.WriteLine("PEN GREEN");
                        }
                        else if (pcommand[0] == ("fillcircle"))
                        {
                            try
                            {
                                x = pcommand[1];
                                int paramX = Convert.ToInt32(x);
                                MyCanvass.FillEllipse(paramX);
                                Console.WriteLine("FILLED CIRCLE");
                            }
                            catch
                            {
                                MessageBox.Show("INVALID FILL-CIRCLE PARAMETERS");
                            }
                        }
                        else if (pcommand[0] == ("fillrectangle"))
                        {
                            try
                            {
                                x = pcommand[1];
                                y = pcommand[2];
                                int paramX = Convert.ToInt32(x);
                                int paramY = Convert.ToInt32(y);
                                MyCanvass.FillRectangle(paramX, paramY);
                                Console.WriteLine("FILLED RECTANGLE");
                            }
                            catch
                            {
                                MessageBox.Show("INVALID FILL-RECTANGLE PARAMETERS");
                            }
                        }
                        else if (pcommand[0] == ("filltriangle"))
                        {
                            MyCanvass.FillTriangle();
                            Console.WriteLine("FILLED TRIANGLE");
                        }
                        else if (pcommand[0] == ("reset"))
                        {
                            MyCanvass.positionReset();
                            Console.WriteLine("RESET PEN POSITION TO INITIAL POINT");
                        }
                        else
                        {
                            Console.WriteLine("INVALID COMMAND");
                            MessageBox.Show("INVALID COMMAND");
                        }
                    }
                }


                else if (Command.Equals("exit") == true)
                {
                    Application.Exit();
                }


                else if (Commands[0] == "drawto")
                {
                    try
                    {
                        x = Commands[1];
                        y = Commands[2];
                        int paramX = Convert.ToInt32(x);
                        int paramY = Convert.ToInt32(y);
                        MyCanvass.DrawTo(paramX, paramY);
                        Console.WriteLine("DRAWN");
                    }
                    catch
                    {
                        MessageBox.Show("INVALID DRAW PARAMETERS");
                    }
                }
                else if (Commands[0] == "moveto")
                {
                    try
                    {
                        x = Commands[1];
                        y = Commands[2];
                        int paramX = Convert.ToInt32(x);
                        int paramY = Convert.ToInt32(y);

                        MyCanvass.MoveTo(paramX, paramY);
                        MyCanvass.PenColourBlack();

                        Console.WriteLine("MOVED");
                    }
                    catch
                    {
                        MessageBox.Show("INVALID MOVE PARAMETERS");
                    }
                }
                else if (Commands[0] == "circle")
                {
                    try
                    {
                        x = Commands[1];
                        int paramX = Convert.ToInt32(x);
                        MyCanvass.DrawEllipse(paramX);
                        Console.WriteLine("CIRCLE");
                    }
                    catch
                    {
                        MessageBox.Show("INVALID CIRCLE PARAMETERS");
                    }
                }
                else if (Commands[0] == "rectangle")
                {
                    try
                    {
                        x = Commands[1];
                        y = Commands[2];
                        int paramX = Convert.ToInt32(x);
                        int paramY = Convert.ToInt32(y);
                        MyCanvass.DrawRectangle(paramX, paramY);
                        Console.WriteLine("RECTANGLE");
                    }
                    catch
                    {
                        MessageBox.Show("INVALID RECTANGLE PARAMETERS");
                    }
                }
                else if (Commands[0] == "triangle")
                {

                    MyCanvass.DrawTriangle();
                    Console.WriteLine("TRIANGLE");
                }
                else if (Commands[0] == "drawto")
                {
                    try
                    {
                        x = Commands[1];
                        y = Commands[2];
                        int paramX = Convert.ToInt32(x);
                        int paramY = Convert.ToInt32(y);
                        MyCanvass.DrawTo(paramX, paramY);
                        Console.WriteLine("drawto");
                    }
                    catch
                    {
                        MessageBox.Show("INVALID DRAW PARAMETERS");
                    }
                }
                else if (Command.Equals("clear") == true)
                {
                    MyCanvass.clearCanvass();
                    Console.WriteLine("GRAPHICS CLEAR");
                }
                else if (Command.Equals("reset") == true)
                {
                    MyCanvass.positionReset();
                    Console.WriteLine("RESET PEN POSITION TO INITIAL POINT");
                }
                else if (Command.Equals("penblue") == true)
                {
                    MyCanvass.PenColourBlue();
                    Console.WriteLine("PEN BLUE");
                }
                else if (Command.Equals("penred") == true)
                {
                    MyCanvass.PenColourRed();
                    Console.WriteLine("PEN RED");
                }
                else if (Command.Equals("penblack") == true)
                {
                    MyCanvass.PenColourBlack();
                    Console.WriteLine("PEN BLACK");
                }
                else if (Command.Equals("pengreen") == true)
                {
                    MyCanvass.PenColourGreen();
                    Console.WriteLine("PEN GREEN");
                }
                else if (Commands[0] == "fillcircle")
                {
                    try
                    {
                        x = Commands[1];
                        int paramX = Convert.ToInt32(x);
                        MyCanvass.FillEllipse(paramX);
                        Console.WriteLine("FILLED CIRCLE");
                    }
                    catch
                    {
                        MessageBox.Show("INVALID FILL-CIRCLE PARAMETERS");
                    }
                }
                else if (Commands[0] == "fillrectangle")
                {
                    try
                    {
                        x = Commands[1];
                        y = Commands[2];
                        int paramX = Convert.ToInt32(x);
                        int paramY = Convert.ToInt32(y);
                        MyCanvass.FillRectangle(paramX, paramY);
                        Console.WriteLine("FILLED RECTANGLE");
                    }
                    catch
                    {
                        MessageBox.Show("INVALID FILL-RECTANGLE PARAMETERS");
                    }
                }
                else if (Commands[0] == "filltriangle")
                {
                    MyCanvass.FillTriangle();
                    Console.WriteLine("FILLED TRIANGLE");
                }
                else
                {
                    Console.WriteLine("INVALID COMMAND");
                    MessageBox.Show("INVALID COMMAND");
                }

                commandLine.Text = "";      //Clearing the textbox for the user.
                Refresh();      //System refreshes display. 
            }

        }
        private void ProgrammingText()
        {
            
        }

        private void OutputPicturebox_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImageUnscaled(MyBitmap, 0, 0);        //Drawing output bitmap at specified size - 0,0 (top left).
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();     //Exit WindowsForm.
        }

        private void runButton_Click(object sender, EventArgs e)
        {
            
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            Stream Stream;
            SaveFileDialog saveFile = new SaveFileDialog();                     

            saveFile.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFile.FilterIndex = 1;
            if (saveFile.ShowDialog() == DialogResult.OK)
            {
                if ((Stream = saveFile.OpenFile()) != null)
                {
                    using (StreamWriter Swriter = new StreamWriter(Stream))
                    {
                        Swriter.Write(ProgrammingTextbox.Text);
                        Swriter.Close();
                    }
                    Stream.Close();
                }
            }
        }
        private void loadButton_Click(object sender, EventArgs e)
        {
            {
                Stream Stream;
                OpenFileDialog LoadFile = new OpenFileDialog();
                if (LoadFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if ((Stream = LoadFile.OpenFile()) != null)
                    {
                        string x = LoadFile.FileName;
                        String FileText = File.ReadAllText(x);
                        ProgrammingTextbox.Text = FileText;
                    }
                }
            }
        }
        private void ProgrammingTextbox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}


