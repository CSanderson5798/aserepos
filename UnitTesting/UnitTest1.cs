using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTesting
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMoveTo()
        {
            //Part 1 Test - Will test the 'moveto' function works correctly via commandline.
        }

        [TestMethod]
        public void TestPositionReset()
        {
            //Part 1 Test - Will test the 'reset' function via the commandline to ensure pen position resets to top right of the bitmap.
        }
        [TestMethod]
        public void TestClearCanvass()
        {
            //Part 1 Test - Will test the 'clear' function via the commandline to ensure bitmap is cleaned for future use.
        }
        [TestMethod]
        public void TestVariableCommands()
        {
            //Part 2 Test - Will test the variable commands that is inputted into the ProgramTextbox.
        }
        [TestMethod]
        public void TestLoopCommands()
        {
            //Part 2 Test - Will test the Loop commands that is inputted into the ProgramTextbox.
        }
        [TestMethod]
        public void TestSyntax()
        {
            //Part 2 Test - Will check the syntax that is inputted into the ProgramTextbox and check for an error/multiple errors.
        }
        [TestMethod]
        public void TestIfStatement()
        {
            //Part 2 Test - Will test the If Statements that is inputted into the ProgramTextbox.
        }
    }
}
